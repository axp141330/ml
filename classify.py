"""
PCAP Image Classification using TensorFlow
"""
import click
import cv2
import tensorflow as tf

CATEGORIES = ["BAD", "GOOD"]


def prepare_image(filepath):
    IMG_SIZE = 100
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)


def process_image(filepath):
    model_1 = tf.keras.models.load_model("1x32x1-100-CNN.model")

    model_2 = tf.keras.models.load_model("3x64x2-100-CNN.model")

    prediction_1 = model_1.predict([prepare_image(filepath)])
    prediction_2 = model_2.predict([prepare_image(filepath)])


    print("Model 1: 1x32x1 = " + CATEGORIES[int(prediction_1[0][0])])
    print("Model 2: 3x64x2 = " +CATEGORIES[int(prediction_2[0][0])])

@click.command()
@click.argument('path', type=click.Path(exists=True))
def main(path):
    process_image(path)

if __name__ == '__main__':
    main()

from scapy.all import *
import cv2
import numpy as np
import os

# This won't work for large pcaps
pcaps = glob('output/good/*.pcap')

for pcap in pcaps:
    filelead = pcap.split(".")[0]
    if os.path.isfile(filelead + '.png'):
        print("File already processed, skipping: {}".format(pcap))
        continue
    try:
        packets = list(rdpcap(pcap))
    except:
        print("Failed to open: {}".format(pcap))
        continue

    packets_bytes = [str(packet) for packet in packets]
    
    height = len(packets_bytes)
    if height != 0:
        width = max([len(packet_bytes) for packet_bytes in packets_bytes])
        
        print(height, width)
        
        canvas = np.empty((height, width, 1))
        canvas[:][:] = 0
        
        for x in range(0, len(packets_bytes)):
            for y in range(0, len(packets_bytes[x])):
                canvas[x][y] = ord(packets_bytes[x][y])
            
        cv2.imwrite(pcap.replace('pcap','png'), canvas)
    else:
        print("Found empty pcap. Removing: {}".format(pcap))
        os.remove(pcap)

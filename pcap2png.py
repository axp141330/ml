"""
PCAP Image Generation using OpenCV
"""
import logging
import os
import click
import cv2
import numpy as np
from scapy.all import *

def process_pcap(path):
    filepath = path.split('.')[0]
    if os.path.isfile(filepath + '.png'):
        logging.warning('File already processed, skipping: %s', path)
        return
    try:
        packets = list(rdpcap(path))
    except:
        logging.error('Failed to open: %s', path)
        return

    packets_bytes = [str(packet) for packet in packets]
    height = len(packets_bytes)
    if height != 0:
        width = max([len(packet_bytes) for packet_bytes in packets_bytes])

        canvas = np.empty((height, width, 1))
        canvas[:][:] = 0

        for x in range(0, len(packets_bytes)):
            for y in range(0, len(packets_bytes[x])):
                canvas[x][y] = ord(packets_bytes[x][y])

        cv2.imwrite(path.replace('pcap','png'), canvas)
        logging.info('Created image with %d x %d size: %s', height, width, path.replace('pcap','png'))
    else:
        logging.warning('PCAP is empty, removing: %s', path)
        os.remove(path)

@click.command()
@click.argument('path', type=click.Path(exists=True))
def main(path):
    process_pcap(path)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()

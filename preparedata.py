import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tqdm import tqdm
import pickle
import random

DATADIR='/Users/alan/Projects/ML/Dataset' # change if you run it locally 
CATEGORIES = ['BAD','GOOD']
IMG_WIDTH = 100
IMG_LENGTH = 100 

training_data_images = []
def load_training_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR,category)  # create path to good and bad 
        class_num = CATEGORIES.index(category)  # get the classification  (0 or 1)
        for img in tqdm(os.listdir(path)):  # iterate over each image per good and bad 
            try:
                img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
                new_array = cv2.resize(img_array, (IMG_WIDTH, IMG_LENGTH))  # resize to normalize data size
                training_data_images.append([new_array, class_num])  # add this to our training_data
            except Exception as e:  
                pass

load_training_data()

print(len(training_data_images))
X = []
y = []

random.shuffle(training_data_images)
for features,label in training_data_images:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_WIDTH, IMG_LENGTH, 1)

pickle_out = open("X_100.pickle","wb")
pickle.dump(X, pickle_out)
pickle_out.close()

pickle_out = open("y_100.pickle","wb")
pickle.dump(y, pickle_out)
pickle_out.close()
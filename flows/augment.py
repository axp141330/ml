import csv
import psycopg2

def augment(filename):
    with open(filename) as csvfile:
        with open('output.csv', 'w') as outfile:
            conn_string = "host='localhost' dbname='unsw' user='postgres'"
            conn = psycopg2.connect(conn_string)
            cursor = conn.cursor()
            reader = csv.DictReader(csvfile)
            features = list(reader.fieldnames)
            features.extend(['srcip', 'dstip', 'Stime', 'Ltime'])
            writer = csv.DictWriter(outfile, fieldnames=features)
            writer.writeheader()
            for row in reader:
                cursor.execute("SELECT srcip, dstip, Stime, Ltime FROM flow WHERE dur='{}' AND Sload='{}' AND Dload='{}'".format(row['dur'], row['sload'], row['dload']))
                records = cursor.fetchone()
                row['srcip'] = records[0][0]
                row['dstip'] = records[0][1]
                row['Stime'] = records[0][2]
                row['Ltime'] = records[0][3]
                writer.writerow(row)

augment('csv/training.csv')

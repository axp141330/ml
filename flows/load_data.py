import csv
import psycopg2

def process_dataset(dataset, testing):
    conn_string = "host='localhost' dbname='unsw' user='postgres'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor()
    flow_number = 1
    with open(dataset, encoding='utf-8-sig') as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            cursor.execute("INSERT INTO data(id, proto, state, service, dur, sload, dload, label, testing) VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}', {}, {})".format(line['id'], line['proto'], line['state'], line['service'], line['dur'], line['sload'], line['dload'], line['label'], testing))
            flow_number += 1
            if flow_number % 100 == 0:
                conn.commit()
                print(flow_number)    
    conn.commit()
    cursor.execute("SELECT COUNT(*) FROM data")
    records = cursor.fetchall()
    print(records)
                

process_dataset('csv/training.csv', 0)
process_dataset('csv/testing.csv', 1)

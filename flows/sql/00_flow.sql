CREATE TABLE flow (
id SERIAL,
srcip VARCHAR(20),
dstip VARCHAR(20),
proto VARCHAR(20),
state VARCHAR(20),
service VARCHAR(20),
dur VARCHAR(20),
sload VARCHAR(20),
dload VARCHAR(20),
stime BIGINT,
ltime BIGINT,
label INT
)

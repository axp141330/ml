import csv
import psycopg2

def load_features(featurespath):
    features = []
    with open(featurespath) as featuresfile:
        featurereader = csv.DictReader(featuresfile)
        for row in featurereader:
            features.append(row['Name'])
    return features

def process_dataset(features, dataset):
    conn_string = "host='localhost' dbname='unsw' user='postgres'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor()
    flow_number = 1
    with open(dataset, encoding='utf-8-sig') as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=features)
        for line in reader:
            cursor.execute("INSERT INTO flow(srcip, dstip, proto, state, service, dur, sload, dload, stime, ltime, label) VALUES ('{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {})".format(line['srcip'], line['dstip'], line['proto'], line['state'], line['service'], line['dur'], line['Sload'], line['Dload'], line['Stime'], line['Ltime'], line['Label']))
            flow_number += 1
            if flow_number % 100 == 0:
                conn.commit()
                print(flow_number)    
    conn.commit()
    cursor.execute("SELECT COUNT(*) FROM flow")
    records = cursor.fetchall()
    print(records)
                

features = load_features('csv/features.csv')
process_dataset(features, 'csv/raw.csv')

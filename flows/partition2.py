import csv
import glob
import os
import subprocess
import arrow
import psycopg2

class PCAPExtractor():

    def __init__(self, pcaps):
        self.load_pcap_times(pcaps)

    def load_pcap_times(self, pcaps):
        """Load pcap times using capinfos to get the start and finish time of each pcap"""
        pcaps = glob.glob(os.path.join(pcaps, "*.pcap"))
        self.pcaps = {}
        for pcap in pcaps:
            info = subprocess.run(['capinfos', '-aeS', pcap], capture_output=True)
            if info.returncode != 0:
                print("Error in return code")
            else:
                stdout = info.stdout.decode('utf-8').split('\n')
                name = stdout[0].split(':')[1].strip()
                startstring = stdout[1].split(':')[1].strip()
                start = arrow.get(startstring)
                endstring = stdout[2].split(':')[1].strip()
                end = arrow.get(endstring)
                self.pcaps[name] = {"start": start.timestamp,
                                   "end": end.timestamp}
        print(self.pcaps)

    def load_features(self, features):
        self.features = []
        with open(features) as featuresfile:
            featurereader = csv.DictReader(featuresfile)
            for row in featurereader:
                self.features.append(row['Name'])

    def find_pcap(self, starttime, endtime):
        for key, value in self.pcaps.items():
            if starttime >= value['start'] and starttime <= value['end']:
                if endtime >= value['start'] and endtime <= value['end']:
                    return key
        return 'nofile'

    def process_dataset(self):
        conn_string = "host='localhost' dbname='unsw' user='postgres'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        done = {}
        # data.id, flow.id AS flowid, srcip, dstip, stime, ltime, data.label, testing
        cursor.execute("SELECT id, srcip, dstip, proto, stime, ltime, label FROM flow WHERE label = 0 AND ltime < 1421931433 AND id > 1453 ORDER BY id")
        records = cursor.fetchall()
        for record in records:
            try:
                done[record[0]]
                print("Skipping done flow: {}".format(record[0]))
            except KeyError:
                print("Processing flow", record)
                starttime = arrow.get(record[4])
                endtime = arrow.get(record[5])
                filename = self.find_pcap(starttime.timestamp, endtime.timestamp)
                ipfilter = "(ip.src == {srcip} and ip.dst == {dstip}) or (ip.dst == {srcip} and ip.src == {dstip})".format(srcip=record[1], dstip=record[2])
                timefilter = "(frame.time_epoch >= {starttime}) and (frame.time_epoch <= {endtime})".format(starttime=starttime.timestamp, endtime=endtime.timestamp+1)
                filterstring = '({ipfilter}) and ({timefilter}) and ({protocol})'.format(ipfilter=ipfilter, timefilter=timefilter, protocol=record[3])
                if record[6] == 0:
                    output = "output/good/{}.pcap".format(record[0])
                else:
                    output = "output/bad/{}.pcap".format(record[0])
                result = subprocess.run(['tshark', '-nr', filename, "-Y", filterstring, "-w", output], capture_output=True)
                print(result)
                done[record[0]] = True

processor = PCAPExtractor('src')
processor.process_dataset()
